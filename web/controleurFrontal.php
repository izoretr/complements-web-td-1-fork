<?php

////////////////////
// Initialisation //
////////////////////

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

/////////////
// Routage //
/////////////

$requete = Request::createFromGlobals();

$res = TheFeed\Controleur\RouteurURL::traiterRequete($requete);

$res->send();
