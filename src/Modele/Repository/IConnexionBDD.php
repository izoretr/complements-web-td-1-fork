<?php

namespace TheFeed\Modele\Repository;

use PDO;

interface IConnexionBDD
{
    public function getPdo(): PDO;
}