<?php

namespace TheFeed\Modele\Repository;

use PDOStatement;
use TheFeed\Modele\DataObject\Utilisateur;

interface IUtilisateurRepo
{
    /**
     * @return Utilisateur[]
     */
    public function recuperer(): array;

    public function recupererParClePrimaire($id): ?Utilisateur;

    public function recupererParLogin($login): ?Utilisateur;

    public function recupererParEmail($email): ?Utilisateur;

    public function ajouter($entite): false|string;

    public function mettreAJour($entite): void;

    public function supprimer($entite): void;

    /**
     * @param PDOStatement $statement
     * @param array $values
     * @return Utilisateur|void
     */
    public function extraireUtilisateur(PDOStatement $statement, array $values);
}