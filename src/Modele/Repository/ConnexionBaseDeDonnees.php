<?php

namespace TheFeed\Modele\Repository;

use TheFeed\Configuration\Configuration;
use TheFeed\Configuration\ConfigurationBDDInterface;
use TheFeed\Configuration\ConfigurationBDDMySQL;
use PDO;

class ConnexionBaseDeDonnees implements IConnexionBDD
{
    private PDO $pdo;
   
    public function getPdo(): PDO
    {
        return $this->pdo;
    }
   
    public function __construct(ConfigurationBDDInterface $config)
    {
        // Connexion à la base de données
        $this->pdo = new PDO(
            $config->getDSN(),
            $config->getLogin(),
            $config->getMotDePasse(),
            $config->getOptions()
        );
   
        // On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}