<?php

namespace TheFeed\Modele\Repository;

use TheFeed\Modele\DataObject\Publication;

interface IPublicationRepo
{
    /**
     * @return Publication[]
     */
    public function getAll(): array;

    /**
     * @param $idUtilisateur
     * @return Publication[]
     */
    public function recupererParAuteur($idUtilisateur): array;

    public function add(Publication $publication): false|string;

    public function get($id): ?Publication;

    public function update(Publication $publication): void;

    public function remove(Publication $publication): void;
}