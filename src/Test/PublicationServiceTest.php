<?php

namespace TheFeed\Test;

use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;

class PublicationServiceTest extends TestCase
{
    private PublicationService $service;
    private PublicationRepository $publicationRepositoryMock;
    private UtilisateurRepository $utilisateurRepositoryMock;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->publicationRepositoryMock = $this->createMock(PublicationRepository::class);
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepository::class);
        $this->service = new PublicationService($this->publicationRepositoryMock, $this->utilisateurRepositoryMock);
    }

    public function testCreerPublicationUtilisateurInexistant()
    {
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(null);

        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Il faut être connecté pour publier un feed");

        $this->service->creerPublication(null, "fake test msg");
    }

    public function testCreerPublicationVide()
    {
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(new Utilisateur());

        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le message ne peut pas être vide!");

        $this->service->creerPublication(null, "");
    }

    public function testCreerPublicationTropGrande()
    {
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(new Utilisateur());

        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage("Le message ne peut pas dépasser 250 caractères!");

        $this->service->creerPublication(null, str_repeat("abcde", 100));
    }

    public function testNombrePublications(): void
    {
        $this->publicationRepositoryMock->method("getAll")->willReturn([new Publication(), new Publication()]);

        $publis = $this->service->recupererPublications();

        $this->assertCount(2, $publis);
    }

    public function testNombrePublicationsUtilisateur()
    {
        $this->publicationRepositoryMock->method("recupererParAuteur")->withAnyParameters()->willReturn([new Publication(), new Publication(), new Publication()]);

        $publis = $this->service->getPublisDeUtilisateur(-1);

        $this->assertCount(3, $publis);
    }

    /**
     * @throws ServiceException
     */
    public function testCreerPublicationValide(): void
    {
        $this->utilisateurRepositoryMock->method("recupererParClePrimaire")->willReturn(Utilisateur::create("giratina07", "Eg", "yaourt@yopmail.com", ""));

        $this->publicationRepositoryMock->method("add")->willReturnCallback(function (Publication $publication) {
            $this->assertEquals("hey mock !", $publication->getMessage());
            $this->assertEquals("yaourt@yopmail.com", $publication->getAuteur()->getEmail());

            return "-5";
        });

        $this->service->creerPublication(-1, "hey mock !");
    }

    /**
     * @throws ServiceException
     */
    public function testDeletePubliNonConnecte(): void
    {
        $this->expectExceptionMessage("Il faut être connecté pour supprimer un feed");

        $this->service->supprimerPublication(-1, null);
    }

    /**
     * @throws ServiceException
     */
    public function testDeletePubliInconnue(): void
    {
        $this->publicationRepositoryMock->method("get")->willReturn(null);

        $this->expectExceptionMessage("Publication inconnue.");

        $this->service->supprimerPublication(-1, -1);
    }

    /**
     * @throws ServiceException
     */
    public function testDeletePubliMauvaisUser(): void
    {
        $fakePubli = Publication::create("a", Utilisateur::create("surskit", "", "", "", 5), 4);
        
        $this->publicationRepositoryMock->method("get")->willReturn($fakePubli);

        $this->expectExceptionMessage("Seul l'auteur de la publication peut la supprimer");

        $this->service->supprimerPublication($fakePubli->getIdPublication(), -1);
    }
    
    /**
     * @throws ServiceException
     */
    public function testDeletePubliFonctionnel(): void
    {
        $fakePubli = Publication::create("a", Utilisateur::create("surskit", "", "", "", 5), 4);
        
        $this->publicationRepositoryMock->method("get")->willReturn($fakePubli);

        $this->publicationRepositoryMock->method("remove")->willReturnCallback(function (Publication $publication) {
            $this->assertEquals("a", $publication->getMessage());
            $this->assertEquals("surskit", $publication->getAuteur()->getLogin());
        });
        
        $this->service->supprimerPublication($fakePubli->getIdPublication(), 5);
    }
}
