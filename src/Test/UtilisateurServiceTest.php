<?php

namespace TheFeed\Test;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\IUtilisateurRepo;
use TheFeed\Service\FileMovingServiceInterface;
use TheFeed\Service\UtilisateurService;

class UtilisateurServiceTest extends TestCase
{
    private UtilisateurService $service;
    private IUtilisateurRepo|MockObject $utilisateurRepositoryMock;
    //Dossier où seront déplacés les fichiers pendant les tests
    private string $dossierPhotoDeProfil = __DIR__ . "/tmp/";
    private FileMovingServiceInterface $fileMovingService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->utilisateurRepositoryMock = $this->createMock(IUtilisateurRepo::class);
        $this->fileMovingService = new TestFileMovingService();
        mkdir($this->dossierPhotoDeProfil);
        $this->service = new UtilisateurService($this->utilisateurRepositoryMock, $this->fileMovingService, $this->dossierPhotoDeProfil);
    }

    public function testCreerUtilisateurPhotoDeProfil()
    {
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "pig.png";
        $donneesPhotoDeProfil["tmp_name"] = "pig.png";
        $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn(null);
        $this->utilisateurRepositoryMock->method("recupererParEmail")->willReturn(null);
        $this->utilisateurRepositoryMock->method("ajouter")->willReturnCallback(function (Utilisateur $utilisateur) {
            self::assertFileExists(__DIR__ . '/tmp/' . $utilisateur->getNomPhotoDeProfil());

            return "-1";
        });
        $this->service->creerUtilisateur("test", "TestMdp123", "test@example.com", $donneesPhotoDeProfil);
    }

    protected function tearDown(): void
    {
        //Nettoyage
        parent::tearDown();
        foreach (scandir($this->dossierPhotoDeProfil) as $file) {
            if ('.' === $file || '..' === $file) continue;
            unlink($this->dossierPhotoDeProfil . $file);
        }
        rmdir($this->dossierPhotoDeProfil);
    }

}
 