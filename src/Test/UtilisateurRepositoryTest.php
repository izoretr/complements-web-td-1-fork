<?php

namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\IConnexionBDD;
use TheFeed\Modele\Repository\IUtilisateurRepo;
use TheFeed\Modele\Repository\UtilisateurRepository;

class UtilisateurRepositoryTest extends TestCase
{
    private static IUtilisateurRepo $utilisateurRepository;

    private static IConnexionBDD $connexionBaseDeDonnees;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$connexionBaseDeDonnees = new ConnexionBaseDeDonnees(new ConfigurationBDDTestUnitaire());
        self::$utilisateurRepository = new UtilisateurRepository(self::$connexionBaseDeDonnees);
    }

    protected function setUp(): void
    {
        parent::setUp();
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (1, 'test', 'test', 'test@example.com', 'test.png')");
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (2, 'test2', 'test2', 'test2@example.com', 'test2.png')");
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        self::$connexionBaseDeDonnees->getPdo()->query("DELETE FROM utilisateurs");
    }


    public function testSimpleNombreUtilisateurs()
    {
        $this->assertCount(2, self::$utilisateurRepository->recuperer());
    }

    public function testRecupParId()
    {
        self::assertEquals('test2', self::$utilisateurRepository->recupererParClePrimaire(2)->getLogin());
    }

    public function testModifier()
    {
        $u = self::$utilisateurRepository->recupererParLogin('test');
        $u->setEmail('ratio@bozo.test');

        self::$utilisateurRepository->mettreAJour($u);

        self::assertEquals('ratio@bozo.test', self::$utilisateurRepository->recupererParLogin('test')->getEmail());
    }
    
    public function testAjouter()
    {
        $u = Utilisateur::create('supertest', '', 'ratio@bozo.test', 'ppTest.png');

        self::$utilisateurRepository->ajouter($u);

        self::assertEquals('ratio@bozo.test', self::$utilisateurRepository->recupererParLogin('supertest')->getEmail());
    }
}
