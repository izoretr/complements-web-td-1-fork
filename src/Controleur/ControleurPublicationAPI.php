<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\IPublicationServ;

class ControleurPublicationAPI extends ControleurGenerique
{
    public function __construct(
        ContainerInterface                $container,
        private readonly IPublicationServ $publicationService
    )
    {
        parent::__construct($container);
    }
    
    #[Route(path: '/api/publications/{idPublication}', name: 'supprimerPublication', methods: ["DELETE"])]
    public function supprimer($idPublication): Response
    {
        try {
            $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
            $this->publicationService->supprimerPublication($idPublication, $idUtilisateurConnecte);
            return new JsonResponse('', Response::HTTP_NO_CONTENT);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }
}
