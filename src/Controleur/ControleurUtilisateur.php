<?php

namespace TheFeed\Controleur;

use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\MessageFlash;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\IPublicationServ;
use TheFeed\Service\IUtilisateurServ;

class ControleurUtilisateur extends ControleurGenerique
{

    public function __construct(ContainerInterface $container, private readonly IUtilisateurServ $serv, private readonly IPublicationServ $publicationServ)
    {
        parent::__construct($container);
    }

    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name: 'afficherPublicationsUtilisateur', methods: ["GET"])]
    public function afficherPublications(string $idUtilisateur): Response
    {
        try {
            $utilisateur = $this->serv->getUtilisateurParId($idUtilisateur);
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger("afficherListe");
        }

        $publications = $this->publicationServ->getPublisDeUtilisateur($idUtilisateur);

        return $this->afficherVue("utilisateur/page_perso.html.twig", [
            "publications" => $publications,
            "login_utilisateur" => $utilisateur->getLogin(),
            "pp" =>  $utilisateur->getNomPhotoDeProfil()
        ], true);
    }

    #[Route(path: '/inscription', name: 'afficherFormulaireInscription', methods: ["GET"])]
    public function afficherFormulaireCreation(): Response
    {
        return $this->afficherVue("utilisateur/inscription.html.twig", [
            "pagetitle" => "Création d'un utilisateur",
            "method" => Configuration::getDebug() ? "get" : "post",
        ], true);
    }

    #[Route(path: '/inscription', name: 'sInscrire', methods: ["POST"])]
    #[NoReturn] public function creerDepuisFormulaire(): RedirectResponse
    {
        try {
            $this->serv->creerUtilisateur(
                $_POST['login'] ?? null,
                $_POST['mot-de-passe'] ?? null,
                $_POST['email'] ?? null,
                $_FILES['nom-photo-de-profil'] ?? null
            );
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger("afficherFormulaireInscription");
        }

        MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");
        return $this->rediriger("afficherListe");
    }

    #[Route(path: '/connexion', name: 'afficherFormulaireConnexion', methods: ["GET"])]
    public function afficherFormulaireConnexion(): Response
    {
        return $this->afficherVue('utilisateur/connexion.html.twig', [
            "pagetitle" => "Formulaire de connexion",
            "method" => Configuration::getDebug() ? "get" : "post",
        ], true);
    }

    #[Route(path: '/connexion', name: 'seConnecter', methods: ["POST"])]
    public function connecter(): RedirectResponse
    {
        try {
            $this->serv->seConnecter(
                $_POST['login'] ?? null,
                $_POST['mot-de-passe'] ?? null
            );
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger("afficherFormulaireConnexion");
        }

        MessageFlash::ajouter("success", "Connexion effectuée.");
        return $this->rediriger("afficherListe");
    }

    #[Route(path: '/deconnexion', name: 'seDeco')]
    public function deconnecter(): RedirectResponse
    {
        try {
            $this->serv->seDeconnecter();
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger("afficherListe");
        }
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        return $this->rediriger("afficherListe");
    }
}
