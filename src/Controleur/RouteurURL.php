<?php

namespace TheFeed\Controleur;

use Exception;
use LogicException;
use RuntimeException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\NoConfigurationException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Loader\AttributeDirectoryLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use TheFeed\Lib\AttributeRouteControllerLoader;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\U;
use Twig\TwigFunction;

class RouteurURL
{
    /**
     * @throws Exception
     */
    public static function traiterRequete(Request $requete): Response
    {
        $attrClassLoader = new AttributeRouteControllerLoader();
        $fileLocator = new FileLocator(__DIR__);
        $routes = (new AttributeDirectoryLoader($fileLocator, $attrClassLoader))->load(__DIR__);

        $container = new ContainerBuilder();
        $container->set('container', $container);
        $container->setParameter('project_root', __DIR__ . '/../..');
        $container->setParameter('chemin_dossier', "%project_root%/ressources/img/utilisateurs");

        //On indique au FileLocator de chercher à partir du dossier de configuration
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . "/../Configuration"));
        //On remplit le conteneur avec les données fournies dans le fichier de configuration
        $loader->load("container.yml");

        $contexteRequete = (new RequestContext())->fromRequest($requete);
        $associateurUrl = new UrlMatcher($routes, $contexteRequete);

        $container->set('routes', $routes);
        $container->set('request_context', $contexteRequete);

        $twig = $container->get('twig');
        $twig->addGlobal('idUtilisateurConnecte', ConnexionUtilisateur::getIdUtilisateurConnecte());
        $twig->addGlobal('messagesFlash', new MessageFlash());
        $twig->addFunction(new TwigFunction("route", $container->get('url_generator')->generate(...)));
        $twig->addFunction(new TwigFunction("asset", $container->get('url_helper')->getAbsoluteUrl(...)));

        try {            
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());

            $requete->attributes->add($donneesRoute);

            $resolveurDeControleur = new ContainerControllerResolver($container);
            $controleur = $resolveurDeControleur->getController($requete);

            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);

            $res = call_user_func_array($controleur, $arguments);
        } catch (NoConfigurationException) {
            $res = $container->get('controleur_generique')->afficherErreur("Aucune configuration trouvée.");
        } catch (ResourceNotFoundException) {
            $res = $container->get('controleur_generique')->afficherErreur("Page non-trouvée !", 404);
        } catch (MethodNotAllowedException $e) {
            $res = $container->get('controleur_generique')->afficherErreur("Méthode non prise en charge : $e", 405);
        } catch (LogicException) {
            $res = $container->get('controleur_generique')->afficherErreur("Problème logique, reportez cet incident aux administrateurs svp");
        } catch (RuntimeException) {
            $res = $container->get('controleur_generique')->afficherErreur("Erreur générique... Désolé");
        }

        return $res;
    }
}
