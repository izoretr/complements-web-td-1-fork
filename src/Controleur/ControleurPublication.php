<?php

namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\IPublicationServ;

class ControleurPublication extends ControleurGenerique
{
    public function __construct(ContainerInterface $container, private readonly IPublicationServ $serv)
    {
        parent::__construct($container);
    }

    #[Route(path: '/', name: 'actionDefault')]
    #[Route(path: '/publications', name: 'afficherListe', methods: ["GET"])]
    public function afficherListe(): Response
    {
        return $this->afficherVue("publication/feed.html.twig", [
            "publications" => $this->serv->recupererPublications(),
            "pagetitle" => "The Feed"
        ], true);
    }

    #[Route(path: '/publications', name: 'publier', methods: ["POST"])]
    public function creerDepuisFormulaire(): Response
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try {
            $this->serv->creerPublication($idUtilisateurConnecte, $message);
        } catch (ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
        }

        return self::rediriger('afficherListe');
    }

}