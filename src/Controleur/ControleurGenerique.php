<?php

namespace TheFeed\Controleur;

use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\MessageFlash;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ControleurGenerique
{
    public function __construct(private readonly ContainerInterface $container)
    {
    }

    /** @return Response réponse HTTP */
    protected function afficherVue(string $cheminVue, array $parametres = [], bool $twig = false): Response
    {
        if ($twig) {
            return self::afficherTwig($cheminVue, $parametres);
        } else {
            $parametres['cheminVueBody'] = $cheminVue;
            return self::afficherHTML($parametres);
        }
    }

    /** @return Response réponse HTTP */
    private function afficherHTML(array $parametres = []): Response
    {
        extract($parametres);

        $messagesFlash = MessageFlash::lireTousMessages();

        ob_start();
        require __DIR__ . "/../vue/vueGenerale.php";
        $blob = ob_get_clean();

        return new Response($blob);
    }

    private function afficherTwig(string $cheminVue, array $parametres = []): Response
    {
        /** @var Environment $twig */
        $twig = $this->container->get("twig");
        try {
            $corpsReponse = $twig->render($cheminVue, $parametres);
        } catch (LoaderError|RuntimeError|SyntaxError $e) {
            echo $e->getMessage();
            die();
        }
        return new Response($corpsReponse);
    }


    #[NoReturn] protected function rediriger(string $route, array $params = []): RedirectResponse
    {
        $generateurUrl = $this->container->get("url_generator");

        $url = $generateurUrl->generate($route, $params);

        return new RedirectResponse($url);
    }

    public function afficherErreur($messageErreur = "", $statusCode = 400): Response
    {
        $reponse = $this->afficherVue('erreur.html.twig', [
            "pagetitle" => "Problème",
            "errorMessage" => $messageErreur
        ], true);

        $reponse->setStatusCode($statusCode);
        return $reponse;
    }

}