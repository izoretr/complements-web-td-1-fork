<?php

namespace TheFeed\Service\Exception;

use Exception;

class ServiceException extends Exception
{
    /**
     * @param string $text
     * @param int $errorCode
     */
    /*
    public function __construct(string $text, int $errorCode)
    {
        parent::__construct($text, $errorCode);
    }
    */
}