<?php

namespace TheFeed\Service;

use Symfony\Component\HttpFoundation\Response;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\IPublicationRepo;
use TheFeed\Modele\Repository\IUtilisateurRepo;
use TheFeed\Service\Exception\ServiceException;

class PublicationService implements IPublicationServ
{
    public function __construct(private readonly IPublicationRepo $repo, private readonly IUtilisateurRepo $utilisateurRepo)
    {
    }

    public function recupererPublications(): array
    {
        return $this->repo->getAll();
    }

    /**
     * @throws ServiceException
     */
    public function creerPublication($idUtilisateur, string $message): void
    {
        $utilisateur = $this->utilisateurRepo->recupererParClePrimaire($idUtilisateur);

        if ($utilisateur == null) {
            throw new ServiceException("Il faut être connecté pour publier un feed");
        }
        if ($message == null || $message == "") {
            throw new ServiceException("Le message ne peut pas être vide!");
        }
        if (strlen($message) > 250) {
            throw new ServiceException("Le message ne peut pas dépasser 250 caractères!");
        }

        $publication = Publication::create($message, $utilisateur);
        $this->repo->add($publication);
    }

    /**
     * @throws ServiceException
     */
    public function supprimerPublication(int $idPublication, ?string $idUtilisateurConnecte): void
    {
        $publication = $this->repo->get($idPublication);

        if (is_null($idUtilisateurConnecte))
            throw new ServiceException("Il faut être connecté pour supprimer un feed", Response::HTTP_UNAUTHORIZED);

        if ($publication === null)
            throw new ServiceException("Publication inconnue.", Response::HTTP_NOT_FOUND);

        if ($publication->getAuteur()->getIdUtilisateur() !== intval($idUtilisateurConnecte))
            throw new ServiceException("Seul l'auteur de la publication peut la supprimer", Response::HTTP_FORBIDDEN);

        $this->repo->remove($publication);
    }

    /**
     * @param int $idUtilisateur
     * @return Publication[]
     */
    public function getPublisDeUtilisateur(int $idUtilisateur): array
    {
        return $this->repo->recupererParAuteur($idUtilisateur);
    }
}