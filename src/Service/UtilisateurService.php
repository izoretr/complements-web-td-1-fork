<?php

namespace TheFeed\Service;

use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\IUtilisateurRepo;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements IUtilisateurServ
{
    public function __construct(
        private readonly IUtilisateurRepo           $repo,
        private readonly FileMovingServiceInterface $fileMovingService,
        private readonly string                     $dossierPhotoDeProfil
    )
    {
    }

    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil): void
    {
        if (!isset($login, $motDePasse, $email, $donneesPhotoDeProfil)) {
            throw new ServiceException("Des valeurs renseignées sont vides !");
        }

        if (strlen($login) < 4 || strlen($login) > 20) {
            throw new ServiceException("Le login doit être compris entre 4 et 20 caractères!");
        }
        if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
            throw new ServiceException("Mot de passe invalide!");
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new ServiceException("L'adresse mail est incorrecte!");
        }

        $utilisateur = $this->repo->recupererParLogin($login);
        if ($utilisateur != null) {
            throw new ServiceException("Ce login est déjà pris!");
        }

        $utilisateur = $this->repo->recupererParEmail($email);
        if ($utilisateur != null) {
            throw new ServiceException("Un compte est déjà enregistré avec cette adresse mail!");
        }

        $mdpHache = MotDePasse::hacher($motDePasse);

        // Upload des photos de profil
        // Plus d'informations :
        // http://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/tut4-complement.html

        // On récupère l'extension du fichier
        $explosion = explode('.', $donneesPhotoDeProfil['name']);
        $fileExtension = end($explosion);
        if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
            throw new ServiceException("La photo de profil n'est pas au bon format!");
        }
        // La photo de profil sera enregistrée avec un nom de fichier aléatoire
        $pictureName = uniqid() . '.' . $fileExtension;
        $from = $donneesPhotoDeProfil['tmp_name'];
        $to = "$this->dossierPhotoDeProfil$pictureName";
        
        $this->fileMovingService->moveFile($from, $to);

        $utilisateur = Utilisateur::create($login, $mdpHache, $email, $pictureName);
        $this->repo->ajouter($utilisateur);
    }

    /**
     * @throws ServiceException
     */
    public function getUtilisateurParId(int $id, $autoriserNull = true): ?Utilisateur
    {
        $utilisateur = $this->repo->recupererParClePrimaire($id);
        if ($utilisateur === null && !$autoriserNull) {
            throw new ServiceException("Utilisateur introuvable..");
        }
        return $utilisateur;
    }

    /**
     * @throws ServiceException
     */
    public function seConnecter(string $login, string $mdp): void
    {
        if (!isset($login, $mdp)) {
            throw new ServiceException("Login ou mot de passe manquant.");
        }

        /** @var Utilisateur $utilisateur */
        $utilisateur = $this->repo->recupererParLogin($login);

        if ($utilisateur == null) {
            throw new ServiceException("Login inconnu.");
        }

        if (!MotDePasse::verifier($mdp, $utilisateur->getMdpHache())) {
            throw new ServiceException("Mot de passe incorrect.");
        }

        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
    }

    /**
     * @throws ServiceException
     */
    public function seDeconnecter(): void
    {
        if (!ConnexionUtilisateur::estConnecte()) {
            throw new ServiceException("Utilisateur non connecté.");
        }
        ConnexionUtilisateur::deconnecter();
    }
}