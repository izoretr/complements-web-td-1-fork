<?php

namespace TheFeed\Service;

class UploadedFileMovingService implements FileMovingServiceInterface
{

    public function moveFile($fileName, $pathDestination): void
    {
        move_uploaded_file($fileName, $pathDestination);
    }
}