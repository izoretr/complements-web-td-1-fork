<?php

namespace TheFeed\Service;

use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Service\Exception\ServiceException;

interface IPublicationServ
{
    public function recupererPublications(): array;

    /**
     * @throws ServiceException
     */
    public function creerPublication($idUtilisateur, string $message): void;

    /**
     * @throws ServiceException
     */
    public function supprimerPublication(int $idPublication, ?string $idUtilisateurConnecte): void;

    /**
     * @param int $idUtilisateur
     * @return Publication[]
     */
    public function getPublisDeUtilisateur(int $idUtilisateur): array;
}