<?php

namespace TheFeed\Service;

use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Service\Exception\ServiceException;

interface IUtilisateurServ
{
    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil): void;

    /**
     * @throws ServiceException
     */
    public function getUtilisateurParId(int $id, $autoriserNull = true): ?Utilisateur;

    /**
     * @throws ServiceException
     */
    public function seConnecter(string $login, string $mdp): void;

    /**
     * @throws ServiceException
     */
    public function seDeconnecter(): void;
}