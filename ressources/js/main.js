/**
 * @param {HTMLElement} button La balise <button> cliquée
 */
function supprimerPublication(button) {
    let idPublication = button.dataset.idPublication;
    let URL = apiBase + "publications/" + idPublication;

    fetch(URL, {method: "DELETE"})
        .then(response => {
            if (response.status === 204) {
                // Plus proche ancêtre <div class="feedy">
                let divFeedy = button.closest("div.feedy");
                divFeedy.remove();
            }
        });
}
